package edu.luc.etl.cs413.android.timer.model.state;

import edu.luc.etl.cs413.android.timer.R;

class AlarmState implements TimerState {

    public AlarmState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionStopAlarm();
        sm.actionReset();
        sm.actionResetTick();
        sm.toStoppedState();
    }

    @Override
    public void onDisplay(Integer InputCount) {}

    @Override
    public void onTick() {
        sm.actionStartAlarm();
        sm.actionStop();    //this is stop clock!
    }


    @Override
    public void updateView() {
        sm.updateUICountTime();
    }

    @Override
    public int getId() {
        return R.string.ALARMING;
    }
}

