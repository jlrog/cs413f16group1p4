package edu.luc.etl.cs413.android.timer.common;

/**
 * A source of UI update events for the stopwatch.
 */

public interface TimerUIUpdateSource { void setUIUpdateListener(TimerUIUpdateListener listener); }