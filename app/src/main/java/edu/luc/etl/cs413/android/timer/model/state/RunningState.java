package edu.luc.etl.cs413.android.timer.model.state;

import edu.luc.etl.cs413.android.timer.R;

class RunningState implements TimerState {

    public RunningState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionStop();        //stops clock
        sm.actionReset();       //stop current running time to 00
        sm.actionResetTick();   //reset tick to 00
        sm.toStoppedState();
    }

    @Override
    public void onDisplay(Integer InputCount) { onTick(); }

    @Override
    public void onTick() { sm.actionDecCounter();
        if (sm.isEmpty()) { sm.toAlarmState(); }
    }

    @Override
    public void updateView() {sm.updateUICountTime();}

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}

